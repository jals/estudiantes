<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('paterno')->nullable();
            $table->string('materno')->nullable();
            $table->tinyInteger('sexo')->nullable();
            $table->tinyInteger('nacimiento')->nullable();
            $table->string('estado_civil')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
         //  $table->dropColumn('paterno');
         //   $table->dropColumn('materno');
         //   $table->dropColumn('sexo');
        //    $table->dropColumn('nacimiento');
            $table->dropColumn('estado_civil');


        });
    }
}
