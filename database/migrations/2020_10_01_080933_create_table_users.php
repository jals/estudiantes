<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
       //     $table->tinyInteger('nacimiento')->nullable();
            $table->tinyInteger('carrera')->nullable();
            $table->tinyInteger('licenciatura')->nullable();
            $table->tinyInteger('posgrado')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('nacimiento');
            $table->dropColumn('carrera');
            $table->dropColumn('licenciatura');
            $table->dropColumn('posgrado');

        });
    }
}
