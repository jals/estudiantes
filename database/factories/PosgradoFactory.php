<?php

namespace Database\Factories;

use App\Models\Posgrado;
use Illuminate\Database\Eloquent\Factories\Factory;

class PosgradoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Posgrado::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
