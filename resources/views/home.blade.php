@extends('layouts.app')

@section('content')
<div class="container mt-5">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header bg-success">{{ __("Bienvenido". ' '.Auth::user()->name) }} </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="container">
                        <div class="text-center">
                            @if(Auth::user()->carrera  === 1)
                                {{__('Preparatoria')}}
                            @elseif(Auth::user()->carrera  === 2 AND Auth::user()->licenciatura  === 1)
                                {{__('Posgrado:  Mtria. de dirección de proyectos')}}

                            @endif
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
