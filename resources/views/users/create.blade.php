@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card  shadow-lg p-3 mb-5 bg-white rounded">
                    <div class="card-header bg-primary">
                        <h3 class="text-center text-white">{{ __('Registro') }}</h3>
                    </div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <!-- Nombres-->
                            <div class="row">
                                <div class="col-xl-12">
                                    <div class="input-group">
                                        <div class="input-group-sm">
                                            <label for="name">{{ __('Nombre(s)') }}</label>

                                        </div>

                                        <div class="col-xl-12 mb-2">
                                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                            @error('name')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>

                                    </div>


                                </div>

                            </div>
                            <!-- End-->


                            <!-- Apellido Paterno y Materno-->
                            <div class="row">

                                <div class="col-lg-6">
                                    <div class="input-group">
                                        <div class="input-group-sm">
                                            <label for="email">{{ __('Apellido Paterno')}}</label>

                                        </div>

                                        <div class="col-lg-12">
                                            <input id="paterno"  type="text" class="form-control" name="paterno" value="{{old('paterno')}}" required autocomplete="">
                                            @error('paterno')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>


                                    </div>
                                </div>

                                <div class="col-lg-6 col-auto">
                                    <div class="input-group">
                                        <div class="input-group-sm">
                                            <label for="email">{{ __('Apellido Materno')}}</label>

                                        </div>

                                        <div class="col-lg-12 mb-5">
                                            <input id="materno"  type="text" class="form-control" name="materno" value="{{old('materno')}}" required autocomplete="">
                                            @error('materno')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>

                                    </div>

                                </div>

                            </div>
                            <!-- End-->

                            <!-- Sexo, edad, Estado civil-->
                            <div class="row">


                                <div class="col-md-4 ">
                                    <div class="input-group">
                                        <div class="input-group-sm">
                                            <label for="email">{{ __('Sexo')}}</label>

                                        </div>

                                        <div class="col-md-12">
                                            <select id="sex" name="sex" class="form-control dropdown">
                                                <option value="0">Selecciona una opción</option>
                                                <option value="1">Masculino</option>
                                                <option value="2">Femenino</option>
                                            </select>

                                            @error('sexo')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>


                                    </div>
                                </div>


                                <div class="col-md-4 ">
                                    <div class="input-group">
                                        <div class="input-group-sm">
                                            <label for="email">{{ __('Edad')}}</label>

                                        </div>
                                        <div class="col-md-12">
                                            <input id="age" type="number" class="form-control" name="age" value="{{old('age')}}">

                                            @error('age')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                    </div>

                                </div>


                                <div class="col-md-4 col-auto mb-5">
                                    <div class="input-group">
                                        <div class="input-group-sm">
                                            <label for="email">{{ __('Estado Civil')}}</label>

                                        </div>

                                        <div class="col-md-12">
                                            <select id="single" name="single" class="form-control dropdown">
                                                <option value="0">Selecciona una opción</option>
                                                <option value="1">Soltero</option>
                                                <option value="2">Casado</option>
                                            </select>

                                            @error('single')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                    </div>

                                </div>

                            </div>
                            <!-- END -->

                            <!-- Email-->
                            <div class="row">
                                <div class="col-lg-12 mb-2">
                                    <div class="input-group">
                                        <div class="input-group-sm">
                                            <label for="email" >{{ __('E-mail') }}</label>

                                        </div>
                                        <div class="col-xl-12">
                                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                            @error('email')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <!-- END -->

                            <!-- Contraseña-->
                            <div class="row">
                                <div class="col-lg-12 mb-2">
                                    <div class="input-group">
                                        <div class="input-group-sm">
                                            <label for="password">{{ __('Contraseña') }}</label>

                                        </div>

                                        <div class="col-xl-12">
                                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                            @error('password')
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- END -->

                            <!-- Confirmar Contraseña -->
                            <div class="row">
                                <div class="col-lg-12 mb-2">
                                    <label for="password-confirm">{{ __('Confirmar Contraseña') }}</label>

                                    <div class="col-xl-12">
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                    </div>
                                </div>
                            </div>
                            <!-- End -->

                            <!-- Nivel de interes-->
                            <div class="row mb-3">
                                <div class="col-lg-12">

                                    <label for="career" >{{ __('Nivel de interés')}}</label>
                                    <div class="col-xl-12">
                                        <select id="career" name="career" class="form-control dropdown">
                                            <option value="0">Selecciona una opción</option>
                                            <option value="1">Preparatoria</option>
                                            <option value="2">Licenciatura</option>
                                            <option value="3">Posgrado</option>
                                        </select>

                                        @error('single')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <!-- End -->


                            <!-- Licenciatur-->
                            <div class="row lic">

                                <div class="col-lg-12">
                                    <label for="licenciatura" >{{ __('Licenciatura')}}</label>
                                    <div class="col-xl-12">
                                        <select id="licenciatura" name="licenciatura" class="form-control dropdown">
                                            <option value="0">Selecciona una opción</option>
                                            <option value="1">Lic. en Derecho</option>
                                            <option value="2">Lic. en Finanzas</option>

                                        </select>

                                        @error('licenciatura')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <!-- End -->

                            <!-- Posgrado-->
                            <div class="row pos">
                                <div class="col-lg-12 mb-5">
                                    <label for="posgrado" >{{ __('Posgrado')}}</label>
                                    <div class="col-xl-12">
                                        <select id="posgrado" name="posgrado" class="form-control dropdown">
                                            <option value="0">Selecciona una opción</option>
                                            <option value="1">Mtria. Admon</option>
                                            <option value="2">Mtria. Dirección de proyectos</option>

                                        </select>

                                        @error('posgrado')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <!-- End-->




                            <!--Btn Save -->
                            <div class="form-group">
                                <div class="col-12 offset-5">
                                    <button type="submit" class="btn-lg btn-success">
                                        {{ __('Registrar') }}
                                    </button>
                                </div>
                            </div>
                            <!-- End-->
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
