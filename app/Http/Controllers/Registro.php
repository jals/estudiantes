<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;


class Registro extends Controller
{
    public function create()
    {
        return  view('users.create');
    }

    public function store(Request $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'paterno' => $data['paterno'],
            'materno' => $data['materno'],
            'sexo' => $data['sex'],
            'nacimiento' => $data['age'],
            'estado_civil' => $data['single'],
            'carrera' => $data['career'],
            'licenciatura' => $data['licenciatura'],
            'posgrado' => $data['posgrado'],
        ]);


    }
}
